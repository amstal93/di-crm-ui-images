# di-crm-ui-ci-images

Docker image used to build DI Automatic CRM UI

## Build image

https://hub.docker.com/repository/docker/mribichich/di-crm-ui-build

### Build

```
$  docker build -t mribichich/di-crm-ui-build images/build
```

### Push

```
$  docker push mribichich/di-crm-ui-build
```

### Use

```
$  docker pull mribichich/di-crm-ui-build:latest
```

## E2E image

https://hub.docker.com/repository/docker/mribichich/di-crm-ui-e2e

### Build

```
$  docker build -t mribichich/di-crm-ui-e2e:chrome images/e2e
```

### Push

```
$  docker push mribichich/di-crm-ui-e2e:chrome
```

### Use

```
$  docker pull mribichich/di-crm-ui-e2e:chrome
```
